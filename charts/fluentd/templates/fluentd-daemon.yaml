apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd
  namespace: {{ $.Release.Namespace }}
  labels:
    k8s-app: fluentd-logging
    version: v1
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    matchLabels:
      k8s-app: fluentd-logging
  updateStrategy:
    # when updating, restart all pods
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 100%
  template:
    metadata:
      labels:
        k8s-app: fluentd-logging
        version: v1
        kubernetes.io/cluster-service: "true"
      annotations:
        # update on configmap change
        checksum/config-map: {{ include (print $.Template.BasePath "/fluentd-config.yaml") . | sha256sum }}
    spec:
      serviceAccount: fluentd
      serviceAccountName: fluentd
      {{ if .Values.output.includeInternal }}
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      - key: dedicated
        value: master
        effect: NoSchedule
      - key: CriticalAddonsOnly
        value: "True"
        effect: NoSchedule
      {{ end }}
      securityContext:
        seLinuxOptions:
          type: "spc_t"
      containers:
      - name: fluentd
        image: {{ .Values.image.name }}:{{ .Values.image.tag }}
        command: ["/bin/sh", "-c", "/etc/fluent/config.d/install-plugins.sh"]
        env:
          - name: FLUENTD_ARGS
            value: --no-supervisor
          - name: OUTPUT_PRODUCER
            value: {{ (required "please provide producer name" .Values.output.producer)}}
          - name: OUTPUT_ENDPOINT
            value: {{ .Values.output.endpoint }}
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: etcfluentd
          mountPath: /etc/fluent/config.d/
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: etcfluentd
        configMap:
          name: fluentd
          defaultMode: 0777
